import { Container, Compare, Clicktag } from 'kocie-komponenty';
import './App.css';
import leftSide from './images/leftSide.jpg';
import rightSide from './images/rightSide.jpg';
import icon from './images/icon.png';

const LeftSide = () => (
  <>
    <Clicktag />
    <img src={leftSide} alt='' />
  </>
);

const RightSide = () => (
  <>
    <Clicktag />
    <img src={rightSide} alt='' />
  </>
);

const Icon = () => <img src={icon} alt='' />;

const App = () => {
  return (
    <Container>
      <Compare leftSide={LeftSide} rightSide={RightSide} icon={Icon} />
    </Container>
  );
};

export default App;
