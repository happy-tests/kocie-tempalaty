/* eslint-disable import/no-extraneous-dependencies */
const { merge } = require('webpack-merge')
const webpack = require('webpack')
const webpackConfiguration = require('./webpack.config')
const environment = require('./environment')

module.exports = merge(webpackConfiguration, {
  mode: 'development',
  stats: {
    preset: 'minimal',
    moduleTrace: true,
    errorDetails: true,
  },
  /* Manage source maps generation process */
  devtool: false,

  /* Development Server Configuration */
  devServer: {
    contentBase: environment.paths.output,
    watchContentBase: true,
    publicPath: '/',
    open: true,
    historyApiFallback: true,
    compress: true,
    overlay: true,
    hot: true,
    watchOptions: {
      poll: 300,
    },
    ...environment.server,
  },

  /* File watcher options */
  watchOptions: {
    aggregateTimeout: 300,
    poll: 300,
    ignored: /node_modules/,
  },
  module: {
    rules: [
      {
        test: /\.((c|sa|sc)ss)$/i,
        use: [
          { loader: 'style-loader', options: { injectType: 'styleTag' } },
          'css-loader',
          'postcss-loader',
          'sass-loader',
        ],
      },
    ],
  },

  /* Additional plugins configuration */
  plugins: [new webpack.SourceMapDevToolPlugin({})],
})
