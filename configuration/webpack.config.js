/**
 * Webpack main configuration file
 */

const path = require('path');
const webpack = require('webpack');

const HTMLWebpackPlugin = require('html-webpack-plugin');
const ImageMinPlugin = require('imagemin-webpack-plugin').default;

const { CleanWebpackPlugin } = require('clean-webpack-plugin');
require('dotenv').config();

const environment = require('./environment');

module.exports = {
  entry: {
    app: path.resolve(environment.paths.source, 'index.js'),
  },
  output: {
    filename: '[name].[contenthash:6].js',
    path: environment.paths.output,
  },
  resolve: {
    alias: {
      react: 'preact/compat',
      'react-dom': 'preact/compat',
      '@src': path.resolve(__dirname, '../src/'),
      '@components': path.resolve(__dirname, '../src/components/'),
      '@pages': path.resolve(__dirname, '../src/pages/'),
      '@primitives': path.resolve(__dirname, '../src/primitives/'),
      '@assets': path.resolve(__dirname, '../src/assets/'),
    },
  },
  module: {
    rules: [
      {
        test: /\.(txt|ical)$/i,
        use: 'raw-loader',
      },
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ['babel-loader'],
      },
      {
        test: /\.(png|gif|jpg|jpeg)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              name: 'images/[name].[contenthash:6].[ext]',
              publicPath: './',
              useRelativePaths: true,
              limit: environment.limits.images,
            },
          },
        ],
      },
      {
        test: /\.(eot|svg|ttf|woff|woff2)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              name: 'fonts/[name].[contenthash:6].[ext]',
              publicPath: './',
              useRelativePaths: true,
              limit: environment.limits.fonts,
            },
          },
        ],
      },
      {
        test: /\.(mp4|webm|ogv)$/,
        use: [
          {
            loader: 'file-loader',
          },
        ],
      },
    ],
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.ENTITY_ID': JSON.stringify(process.env.ENTITY_ID),
    }),
    new ImageMinPlugin({ test: /\.(jpg|jpeg|png|gif|svg)$/i }),
    new CleanWebpackPlugin({
      verbose: true,
    }),

    new HTMLWebpackPlugin({
      inject: true,
      hash: false,
      scriptLoading: 'defer',
      filename: 'index.html',
      meta: {
        viewport:
          'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no',
        'X-UA-Compatible': 'ie=edge',
      },
    }),
    new webpack.ProvidePlugin({
      h: ['preact', 'h'],
    }),
  ],
  target: 'web',
};
