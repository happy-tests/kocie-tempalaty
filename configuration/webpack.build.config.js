/* eslint-disable import/no-extraneous-dependencies */
const path = require('path');
const { merge } = require('webpack-merge');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const webpackConfiguration = require('./webpack.config');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HTMLInlineCSSWebpackPlugin =
  require('html-inline-css-webpack-plugin').default;
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ZipPlugin = require('zip-webpack-plugin');
const environment = require('./environment');

module.exports = merge(webpackConfiguration, {
  mode: 'production',

  /* Manage source maps generation process. Refer to https://webpack.js.org/configuration/devtool/#production */
  devtool: false,

  /* Optimization configuration */
  optimization: {
    minimize: true,
    minimizer: [
      new TerserPlugin({
        parallel: true,
      }),
      new CssMinimizerPlugin(),
    ],
  },

  /* Performance treshold configuration values */
  performance: {
    maxEntrypointSize: 512000,
    maxAssetSize: 512000,
  },

  module: {
    rules: [
      {
        test: /\.((c|sa|sc)ss)$/i,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          'postcss-loader',
          'sass-loader',
        ],
      },
    ],
  },

  /* Additional plugins configuration */
  plugins: [
    new CopyWebpackPlugin({
      patterns: [
        {
          from: path.resolve(environment.paths.public, 'assets'),
          to: path.resolve(environment.paths.output, 'assets'),
          toType: 'dir',
          globOptions: {
            ignore: ['*.DS_Store', 'Thumbs.db'],
          },
          noErrorOnMissing: true,
        },
      ],
    }),
    new CompressionPlugin({
      test: /\.(js|jsx|css|html)(\?.*)?$/i,
      filename: '[file].gz',
      deleteOriginalAssets: false,
    }),
    new MiniCssExtractPlugin({
      filename: '[name].[contenthash:6].css',
    }),
    new HTMLInlineCSSWebpackPlugin(),
    new ZipPlugin({
      filename: `build--${new Date().toISOString()}.zip`,
    }),
  ],
});
