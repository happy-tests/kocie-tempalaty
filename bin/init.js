#!/usr/bin/env node
const { execSync } = require('child_process');

const runCommand = command => {
  try {
    execSync(`${command}`, { stdio: 'inherit' });
  } catch (e) {
    console.error('Failed to execute ${command}', e);
    return false;
  }
};

const repoName = process.argv[2];

runCommand(
  `git clone --depth 1 git@gitlab.com:happy-pixel/kocia-kreacja.git ${repoName}`,
);
runCommand(`cd ${repoName} && npm install --silent`);
runCommand(`cd ${repoName} && rm -rf .git`);
runCommand(`cd ${repoName} && rm -rf README.md`);
runCommand(`cd ${repoName} && echo "ENTITY_ID=\"${repoName}\"" > .env`);
runCommand(
  `cd ${repoName} && git init && git add . && git commit -m"initial commit"`,
);
